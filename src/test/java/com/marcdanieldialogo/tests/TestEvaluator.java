package com.marcdanieldialogo.tests;

import com.marcdanieldialogo.DynamicArray;
import com.marcdanieldialogo.Evaluator;
import com.marcdanieldialogo.interfaces.MyQueue;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test class meant to test the Evaluator class.
 * @author Marc-Daniel
 */
@RunWith(Parameterized.class)
public class TestEvaluator 
{
    @Parameters
    public static Collection<String []> data() 
    {
        // [0] to [6] are the operands/operators that the tests will use to build infix expressions.
        // [7] is the expected postfix.
        // [8] is the expected evaluated result of the postfix.
        return Arrays.asList(new String[][]
        {
            {"2", "+", "4", "*", "3", "/", "3", "243*3/+", "6.0"},
            {"4", "-", "6", "/", "2", "+", "5", "462/-5+", "6.0"},
            {"6", "*", "2", "-", "6", "-", "4", "62*6-4-", "2.0"},
            {"1", "+", "2", "+", "3", "+", "4", "12+3+4+", "10.0"},
            {"1", "-", "2", "-", "3", "-", "4", "12-3-4-", "-8.0"},
            {"5", "/", "5", "*", "9", "+", "1", "55/9*1+", "10.0"},
            {"6", "*", "3", "-", "0", "*", "3", "63*03*-", "18.0"},
            {"1", "/", "5", "+", "8", "+", "7", "15/8+7+", "15.2"},
            {"5", "-", "3", "/", "3.2", "/", "1", "533.2/1/-", "4.0625"},
            {"6", "+", "4", "-", "3", "+", "3", "64+3-3+", "10.0"},
            {"73", "+", "4", "-", "3", "*", "30", "734+330*-", "-13.0"},
            {"10", "*", "9", "*", "2", "/", "3", "109*2*3/", "60.0"},
            {"8", "/", "3", "*", "1.2", "+", "2.4", "83/1.2*2.4+", "5.6"},
            {"1", "+", "3", "+", "3", "+", "7", "13+3+7+", "14.0"},
            {"1", "*", "4", "*", "7", "*", "5", "14*7*5*", "140.0"},
            {"2", "/", "4", "/", "5", "/", "1", "24/5/1/", "0.1"},
            {"2", "+", "6", "-", "3", "-", "6", "26+3-6-", "-1.0"},
            {"1", "/", "2", "*", "2", "/", "4", "12/2*4/", "0.25"},
            {"2", "-", "4", "+", "5", "-", "3", "24-5+3-", "0.0"},
            {"0", "/", "1", "*", "900", "+", "23.43", "01/900*23.43+", "23.43"},
            {"1.4", "+", "2.5", "-", "2.3", "*", "1.1", "1.42.5+2.31.1*-", "1.37"},
            {"69", "+", "1", "*", "2", "*", "6", "6912*6*+", "81.0"},
            {"0", "-", "564", "*", "3", "+", "11", "05643*-11+", "-1681.0"},
            {"46", "*", "23", "*", "54", "*", "23", "4623*54*23*", "1314036.0"},
            {"1.1", "/", "2", "/", "2", "+", "7.38", "1.12/2/7.38+", "7.655"}
        });
    }
    
    String operaOne, operaTwo, operaThree, operaFour, operaFive, operaSix, operaSeven;
    String expectedPostfix, expectedResult;
    
    /**
     * Constructor for parameterized testing.
     * 
     * @param operaOne
     * @param operaTwo
     * @param operaThree
     * @param operaFour
     * @param operaFive
     * @param operaSix
     * @param operaSeven
     * @param expectedPostfix
     * @param expectedResult 
     */
    public TestEvaluator(String operaOne, String operaTwo, String operaThree, String operaFour, String operaFive, String operaSix, String operaSeven,
            String expectedPostfix, String expectedResult)
    {
        this.operaOne = operaOne;
        this.operaTwo = operaTwo;
        this.operaThree = operaThree;
        this.operaFour = operaFour;
        this.operaFive = operaFive;
        this.operaSix = operaSix;
        this.operaSeven = operaSeven;
        this.expectedPostfix = expectedPostfix;
        this.expectedResult = expectedResult;
    }
    
    /**
     * Tests to see if the Evaluator is able to convert infix to postfix.
     */
    @Test
    public void testInfixToPostfix()
    {
        MyQueue<String> expressionQueue = new DynamicArray<>();
        expressionQueue.add(operaOne);
        expressionQueue.add(operaTwo);
        expressionQueue.add(operaThree);
        expressionQueue.add(operaFour);
        expressionQueue.add(operaFive);
        expressionQueue.add(operaSix);
        expressionQueue.add(operaSeven);
        
        Evaluator evaluator = new Evaluator(expressionQueue);
        
        assertEquals(expectedPostfix, evaluator.infixToPostFix().toString());
    }
    
    /**
     * Tests to see if the Evaluator is able to get the correct result from a postfix.
     */
    @Test
    public void testEvaluatePostfix()
    {
        MyQueue<String> expressionQueue = new DynamicArray<>();
        expressionQueue.add(operaOne);
        expressionQueue.add(operaTwo);
        expressionQueue.add(operaThree);
        expressionQueue.add(operaFour);
        expressionQueue.add(operaFive);
        expressionQueue.add(operaSix);
        expressionQueue.add(operaSeven);
        
        Evaluator evaluator = new Evaluator(expressionQueue);
        MyQueue<String> postfix = evaluator.infixToPostFix();
        
        assertEquals(expectedResult, evaluator.solvePostfixExpression(postfix).toString());
    }
}
