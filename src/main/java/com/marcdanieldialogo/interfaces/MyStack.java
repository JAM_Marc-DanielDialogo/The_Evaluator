/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marcdanieldialogo.interfaces;

/**
 *
 * @author Marc-Daniel
 */
public interface MyStack<T>
{
    T pop();
    void push(T item);
    T peek();
    int size();
}
