package com.marcdanieldialogo;

import com.marcdanieldialogo.interfaces.MyQueue;
import com.marcdanieldialogo.interfaces.MyStack;

/**
 * A generic dynamic array class that implements custom stack and queue interfaces.
 * 
 * @author Marc-Daniel
 */
public class DynamicArray<T> implements MyStack<T>, MyQueue<T>
{
    // The array
    private Object[] array;
    
    // The amount of elements within the array, as well as the current capacity.
    private int size;
    
    /**
     * Constructor that instantiates the array and size according to the T parameters.
     * @param <T>
     * @param params 
     */
    public <T>DynamicArray(T... params)
    {
        size = params.length;
        array = params;
    }
    
    /**
     * Constructor that simply instantiates the array and size with 0 values.
     * @param <T> 
     */
    public <T>DynamicArray()
    {
        size = 0;
        array = new Object[0];
    }
    
    /**
     * Adds an item to the end of the array and resizes.
     * @param t 
     */
    public void add(T t)
    {
        int sizeWithAdd = size+1;
        Object[] arrayWithAdd = new Object[sizeWithAdd];
        
        for(int i = 0; i < size; i++)
        {
            arrayWithAdd[i] = array[i];
        }
        
        arrayWithAdd[sizeWithAdd-1] = t;
        
        array = arrayWithAdd;
        size = sizeWithAdd;
    }
    
    /**
     * Adds an item to the array at a specified position and resizes.
     * @param pos
     * @param t 
     */
    public void add(int pos, T t)
    {
        int sizeWithAdd = size+1;
        Object[] arrayWithAdd = new Object[sizeWithAdd];
        
        for(int i = 0; i < pos; i++)
        {
            arrayWithAdd[i] = array[i];
        }
        
        arrayWithAdd[pos] = t;
        
        for(int i = pos; i < size; i++)
        {
            arrayWithAdd[i+1] = array[i];
        }
        
        array = arrayWithAdd;
        size = sizeWithAdd;
    }
    
    /**
     * Removes the last element of the array and resizes.
     * @return 
     */
    public T remove()
    {
        int sizeWithRemove = size-1;
        Object[] arrayWithRemove = new Object[sizeWithRemove];
        
        for(int i = 0; i < size-1; i++)
        {
            arrayWithRemove[i] = array[i];
        }
        
        T deletedItem = (T) array[size-1];
        
        array = arrayWithRemove;
        size = sizeWithRemove;
        
        return deletedItem;
    }
    
    /**
     * Removes the array element at the specified position and resizes.
     * @param pos
     * @return 
     */
    public T remove(int pos)
    {
        if(pos == size-1)
            return remove();
        else
        {
            int sizeWithRemove = size-1;
            Object[] arrayWithRemove = new Object[sizeWithRemove];

            for(int i = 0; i < pos; i++)
            {
                arrayWithRemove[i] = array[i];

            }

            T deletedItem = (T) array[pos];
            
            for(int i = pos; i < sizeWithRemove; i++)
            {
                arrayWithRemove[i] = array[i+1];

            }

            array = arrayWithRemove;
            size = sizeWithRemove;

            return deletedItem;
        }
    }
    
    /**
     * Sets an item to the array at the specified position. The previous item at the specified position gets overwritten.
     * @param pos
     * @param t 
     */
    public void set(int pos, T t)
    {
        array[pos] = t;
    }
    
    /**
     * Returns the item of the array at the specified position.
     * @param pos
     * @return 
     */
    public T get(int pos)
    {
        return (T) array[pos];
    }
    
    /**
     * Override toString
     * @return 
     */
    @Override
    public String toString()
    {
        String str = "";
        
        if(size > 0)
        {
            for(int i = 0; i < size; i++)
            {
                str += array[i].toString();
            }
        }
        
        return str;
    }

    /**
     * Implementation of the stack pop method.
     * @return 
     */
    @Override
    public T pop() 
    {
        int sizeWithPop = size-1;
        Object[] arrayWithPop = new Object[sizeWithPop];
        
        T poppedItem = (T) array[size-1];
        
        for(int i = 0; i < sizeWithPop; i++)
        {
            arrayWithPop[i] = array[i];
        }
        
        size = sizeWithPop;
        array = arrayWithPop;
        
        return poppedItem;
    }

    /**
     * Implementation of the stack push method.
     * @param item 
     */
    @Override
    public void push(T item) 
    {
        int sizeWithPush = size+1;
        Object[] arrayWithPush = new Object[sizeWithPush];
        
        for(int i = 0; i < size; i++)
        {
            arrayWithPush[i] = array[i];
        }
        arrayWithPush[size] = item;
        
        size = sizeWithPush;
        array = arrayWithPush;
    }

    /**
     * Implementation of the stack peek method.
     * @return 
     */
    @Override
    public T peek() 
    {
        return (T) array[size-1];
    }

    /**
     * Implementation of the stack size method.
     * @return 
     */
    @Override
    public int size() 
    {
        return size;
    }

    /**
     * Implementation of the queue element method.
     * @return 
     */
    @Override
    public T element() 
    {
        return (T) array[0];
    }

    /**
     * Implementation of the queue remove method.
     * @return 
     */
    @Override
    public T queueRemove() 
    {
        int sizeWithRemove = size-1;
        Object[] arrayWithRemove = new Object[sizeWithRemove];
        
        T removedItem = (T) array[0];
        
        for(int i = 0; i < sizeWithRemove; i++)
        {   
            arrayWithRemove[i] = array[i+1];
        }
        
        size = sizeWithRemove;
        array = arrayWithRemove;
        
        return removedItem;
    }
}
