/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marcdanieldialogo;

import com.marcdanieldialogo.interfaces.MyQueue;
import com.marcdanieldialogo.interfaces.MyStack;

/**
 * Evaluator class that handles converting an infix expression to postfix,
 * as well as getting the correct result from a postfix.
 * 
 * @author Marc-Daniel
 */
public class Evaluator 
{
    private MyQueue<String> expressionQueue; 
    
    public Evaluator(MyQueue<String> expressionQueue)
    {
        this.expressionQueue = expressionQueue;
    }
    
    /**
     * Takes this class' expression queue, converts it into postfix, and returns it.
     * @return 
     */
    public MyQueue<String> infixToPostFix()
    {
        MyQueue<String> postfixQueue = new DynamicArray<>();
        MyStack<String> operatorStack = new DynamicArray<>();
        
        while(expressionQueue.size() > 0)
        {
            String removedString = expressionQueue.queueRemove();
            
            // If the removed String is an operand and not an operator, add it to the postfix queue.
            if(!removedString.equals("+") && !removedString.equals("-") && !removedString.equals("*") && !removedString.equals("/"))
            {
                postfixQueue.add(removedString);
            }
            else
            {
                // Push the string into the operator stack, if it's empty.
                if(operatorStack.size() == 0)
                {
                    operatorStack.push(removedString);
                }
                else
                {
                    if(operatorGreaterThan(removedString, operatorStack.peek()))
                    {
                        operatorStack.push(removedString);
                    }
                    else
                    {
                        String pop = operatorStack.pop();
                        
                        postfixQueue.add(pop);
                        
                        operatorStack.push(removedString);
                    }
                    
                    if(operatorStack.size() > 1)
                    {
                        String pop = operatorStack.pop();
                        
                        if(operatorGreaterThan(removedString, operatorStack.peek()))
                        {
                            operatorStack.push(pop);
                        }
                        else
                        {
                            postfixQueue.add(operatorStack.peek());
                            operatorStack.pop();
                            operatorStack.push(pop);
                        }
                    }
                }
            }
        }
        
        // When the Infix Queue is empty pop all the remaining operators in the Operator Stack to the Postfix Queue
        int stackCapacity = operatorStack.size();
        for(int i = 0; i < stackCapacity; i++)
        {
            postfixQueue.add(operatorStack.pop());
        }
        
        return postfixQueue;
    }
    
    /**
     * Compares two operators for precedence.
     * 
     * @param operator1
     * @param operator2
     * @return 
     */
    private boolean operatorGreaterThan(String operator1, String operator2)
    {
        if((operator1.equals("*") || operator1.equals("/")) && (operator2.equals("+") || operator2.equals("-")))
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * Takes in a postfix queue and solves it. Then returns a stack containing the result.
     * @param postfixQueue
     * @return 
     */
    public MyStack<String> solvePostfixExpression(MyQueue<String> postfixQueue)
    {
        MyStack<String> operandStack = new DynamicArray<>();
        
        DynamicArray<String> expressionArray = new DynamicArray<>();
        
        
        while(postfixQueue.size() > 0)
        {
            String removedString = postfixQueue.queueRemove();
            
            if(!removedString.equals("+") && !removedString.equals("-") && !removedString.equals("*") && !removedString.equals("/"))
            {
                operandStack.push(removedString);
            }
            else
            {
                String operandOne = operandStack.pop();
                String operandTwo = operandStack.pop();
                
                operandStack.push(evaluateExpression(operandTwo, removedString, operandOne));
                
            }
        }
        
        return operandStack;
    }
    
    /**
     * Evaluates a simple operand-operator-operand expression.
     * 
     * @param operand1
     * @param operator
     * @param operand2
     * @return 
     */
    private String evaluateExpression(String operand1, String operator, String operand2)
    {
        Double result;
        
        switch(operator)
        {
            case "+": 
                result = Double.parseDouble(operand1) + Double.parseDouble(operand2);
                return String.valueOf(result);
            case "-":
                result = Double.parseDouble(operand1) - Double.parseDouble(operand2);
                return String.valueOf(result);
            case "*":
                result = Double.parseDouble(operand1) * Double.parseDouble(operand2);
                return String.valueOf(result);
            case "/":
                Double one = Double.parseDouble(operand1);
                Double two = Double.parseDouble(operand2);
                
                result = Double.parseDouble(operand1) / Double.parseDouble(operand2);
                
                return String.valueOf(result);
            default:
                return "-1";
        }
    }
}
